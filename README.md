# gl364606/gl364606

## What is this repository?

This repository demonstrates that the same [project composer.json](https://gitlab.com/gl364606/gl364606/-/blob/main/assets/demo-composer.json) will successfully apply patches using [cweagans/composer-patches](https://github.com/cweagans/composer-patches/) when a [dependency component](https://gitlab.com/gl364606/gl364606/-/blob/main/composer.json) (this repo) is required using a VCS repository ([passing job](https://gitlab.com/gl364606/gl364606/-/jobs/2560038136)), and it will fail when the same dependency component is requird using Gitlab's Composer package registry ([failing job](https://gitlab.com/gl364606/gl364606/-/jobs/2560038140)).

The underlying cause of this appears to be that Gitlab's Packagist repository re-orders the JSON content of the dependency component's package information, with undesirable effects in tools such as cweagans/composer-patches.

- [gitlab-org/gitlab#364606: Composer package registry re-orders package data](https://gitlab.com/gitlab-org/gitlab/-/issues/364606)

## More details

Perhaps related Gitlab issues:

- https://gitlab.com/groups/gitlab-org/-/epics/6817
- https://gitlab.com/gitlab-org/gitlab/-/issues/290007
- https://gitlab.com/gitlab-org/gitlab/-/issues/360739#note_938217041

Discussion(s):

- https://drupal.slack.com/archives/CGKLP028K/p1654651616054849
- https://drupal.slack.com/archives/C45SW3FLM/p1654651214660889

Reproduction on `gitlab.com`:

- https://gitlab.com/api/v4/group/54151172/-/packages/composer/xurizaemon/gitlabsortorder%248d74dd74c36c2a6a5db97a2309e71fea29e62144.json
- https://gitlab.com/gl364606/gl364606/-/blob/main/composer.json

Reproduction on `git.fotocadeau.nl` hosted repo:

- https://git.fotocadeau.nl/packages/data/-/blob/1.17.62/composer.json
- https://git.fotocadeau.nl/api/v4/group/21/-/packages/composer/fotocadeau/data%2440796b83b6f99ef8cedeaa887b35da83804af8c9.json
